package main

import (
	b64 "encoding/base64"
	"fmt"
	"strings"
)

func customEncoding() {
	data := "hello world"
	shift := 7

	sEnc := b64.StdEncoding.EncodeToString([]byte(data))
	fmt.Println(sEnc)

	// Trim '=' characters from the string
	sEnc = strings.ReplaceAll(sEnc, "=", "")
	fmt.Println(sEnc)

	// Move 3 caracters
	sEnc = sEnc[shift:] + sEnc[:shift]
	fmt.Println(sEnc)

	// Move them back
	sEnc = sEnc[len(sEnc)-shift:] + sEnc[:len(sEnc)-shift]
	fmt.Println(sEnc)

	sDec, _ := b64.StdEncoding.DecodeString(sEnc)
	fmt.Println(string(sDec))
	fmt.Println()

}
