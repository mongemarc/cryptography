package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func createHash(key string) string {
	hasher := md5.New()
	hasher.Write([]byte(key))
	return hex.EncodeToString(hasher.Sum(nil))
}

func encrypt(data []byte, passphrase string) []byte {
	block, _ := aes.NewCipher([]byte(createHash(passphrase)))
	gcm, _ := cipher.NewGCM(block)
	nonce := make([]byte, gcm.NonceSize())
	io.ReadFull(rand.Reader, nonce)
	ciphertext := gcm.Seal(nonce, nonce, data, nil)
	return ciphertext
}

func encryptFile(filename string, data []byte, passphrase string) {
	f, _ := os.Create(filename)
	defer f.Close()
	f.Write(encrypt(data, passphrase))
}

func decrypt(data []byte, passphrase string) []byte {
	block, _ := aes.NewCipher([]byte(createHash(passphrase)))
	gcm, _ := cipher.NewGCM(block)
	nonceSize := gcm.NonceSize()
	nonce, ciphertext := data[:nonceSize], data[nonceSize:]
	plaintext, _ := gcm.Open(nil, nonce, ciphertext, nil)
	return plaintext
}

func decryptFile(filename string, passphrase string) []byte {
	data, _ := ioutil.ReadFile(filename)
	return decrypt(data, passphrase)
}

func customXor(buffer []byte, password string) []byte {
	passwordBytes := []byte(password)
	result := make([]byte, len(buffer))

	for i, b := range buffer {
		passwordByteIteration := i % len(passwordBytes)
		result[i] = b ^ passwordBytes[passwordByteIteration]
	}
	return result
}

func main() {
	fmt.Println("Hash for AA: ", createHash("AA"))
	cipher := encrypt([]byte("XX"), "AA")
	fmt.Println("Cipher XX with passphrase AA: ", string(cipher))
	fmt.Println("Decrypt chunk with passphrase AA: ", string(decrypt(cipher, "AA")))

	encryptFile("cipher.bin", []byte("hello, world!"), "AA")
	fmt.Println("File decrypted: ", string(decryptFile("cipher.bin", "AA")))

	cipherBytes := customXor([]byte("somestring"), "pass")
	fmt.Println("String is 'thing' and converted bytes are: ", cipherBytes)
	fmt.Println("Back again: ", string(customXor(cipherBytes, "pass")))
}
